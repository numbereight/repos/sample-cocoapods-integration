//
//  PublicNumberEightWrapper.h
//  SampleFramework
//
//  Created by Matthew Paletta on 2021-09-03.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PublicNumberEightWrapper : NSObject

-(void)doTheThing:(nullable NSDictionary<UIApplicationLaunchOptionsKey,id> *)launchOptions shouldStartAudiences:(BOOL)shouldStartAudiences;

@end

NS_ASSUME_NONNULL_END
