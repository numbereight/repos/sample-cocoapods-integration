//
//  PublicNumberEightWrapper.m
//  SampleFramework
//
//  Created by Matthew Paletta on 2021-09-03.
//

#import "PublicNumberEightWrapper.h"

@import NumberEightCompiled;
@import Audiences;

@implementation PublicNumberEightWrapper

-(void)doTheThing:(nullable NSDictionary<UIApplicationLaunchOptionsKey,id> *)launchOptions shouldStartAudiences:(BOOL)shouldStartAudiences {
    NEXAPIToken* token = [NEXNumberEight startWithApiKey:nil launchOptions:launchOptions consentOptions:[NEXConsentOptions withConsentToAll] completion:^(BOOL isSuccess, NSError * _Nullable error) {
        if (!isSuccess) {
            NSLog(@"NumberEight failed to start: %@", error);
        } else {
            NSLog(@"NumberEight started successfully.");
        }
    }];

    if (shouldStartAudiences) {
        NSLog(@"Starting Audiences");
//        [NEXInsights startRecordingWithAPIToken:token config:[NEXRecordingConfig defaultConfig] onStart:^(BOOL, NSError * _Nullable) {
//
//        }];
        [NEXAudiences startRecordingWithApiToken:token];
    } else {
        NSLog(@"Not starting Audiences");
    }
}

@end
