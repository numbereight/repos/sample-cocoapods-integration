set -eou pipefail

pushd SampleFramework
pod install
popd

scheme="SampleFramework"
configuration="Release"

sdk="iphoneos"
output="${scheme}_${sdk}_${configuration}"
xcodebuild archive BUILD_LIBRARY_FOR_DISTRIBUTION=YES SKIP_INSTALL=NO -parallelizeTargets -workspace SampleFramework/SampleFramework.xcworkspace -scheme $scheme -configuration ${configuration} -sdk ${sdk} -archivePath ${output} | xcpretty

sdk="iphonesimulator"
output="${scheme}_${sdk}_${configuration}"
xcodebuild archive -parallelizeTargets -workspace SampleFramework/SampleFramework.xcworkspace -scheme $scheme -configuration ${configuration} -sdk ${sdk} -archivePath ${output} BUILD_LIBRARY_FOR_DISTRIBUTION=YES SKIP_INSTALL=NO | xcpretty

rm -rf Frameworks/${scheme}.xcframework

xcodebuild -create-xcframework -archive ${scheme}_iphoneos_${configuration}.xcarchive -framework ${scheme}.framework -archive ${scheme}_iphonesimulator_${configuration}.xcarchive -framework ${scheme}.framework -output Frameworks/${scheme}.xcframework

rm -rf ${scheme}_iphoneos_${configuration}.xcarchive
rm -rf ${scheme}_iphonesimulator_${configuration}.xcarchive

zip -r SampleFramework.xcframework.zip Frameworks dummy.txt

# pod lib lint SampleFrameworkPreBuilt.podspec --sources="https://gitlab.com/numbereight/repos/Podspecs,https://github.com/CocoaPods/Specs.git"
