//
//  TestAppApp.swift
//  TestApp
//
//  Created by Matthew Paletta on 2021-09-13.
//

import SwiftUI
import SampleFramework

@main
struct TestAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
