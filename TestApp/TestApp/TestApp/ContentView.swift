//
//  ContentView.swift
//  TestApp
//
//  Created by Matthew Paletta on 2021-09-13.
//

import SwiftUI
import SampleFramework

struct ContentView: View {
    var body: some View {
        Text(self.doTheThing())
            .padding()
    }

    func doTheThing() -> String {
        let wrapper = PublicNumberEightWrapper()
        wrapper.doTheThing(nil, shouldStartAudiences: true)

        return "Hello, world!"
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
