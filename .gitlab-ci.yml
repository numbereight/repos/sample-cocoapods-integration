include:
  - project: 'numbereight/operations/ci-scripts'
    file: '/ios/ci.yml'

stages:
  - prepare
  - fetch
  - build
  - update
  - lint
  - deploy

variables:
  GIT_DEPTH: 1
  SAMPLE_FRAMEWORK_PODSPEC: SampleFrameworkPreBuilt.podspec
  SAMPLE_FRAMEWORK_APP_PODFILE: SampleFramework/Podfile

#
# Gets the job ID from the upstream pipeline.  See `Update Version` job for more information.
#
Get Assemble NumberEightCompiled (Debug) Job ID:
  extends: .base-get-job-id-ios
  stage: prepare
  variables:
    NESDKIOS_DEPENDENT_JOB_NAME: "Assemble NumberEightCompiled (Debug)"

#
# Downloads the artifacts from the upstream pipeline for the particular job ID.
# This will make them available as artifacts to downstream jobs of this pipeline.
#
Get Artifacts NumberEightCompiled:
  extends: .base-get-job-artifacts-ios
  stage: fetch
  needs:
    - job: Get Assemble NumberEightCompiled (Debug) Job ID
      artifacts: true
  artifacts:
    paths:
      - NumberEightCompiled_Debug.xcframework

#
# Builds SampleFramework with the current SDK version.
#
Build SampleFramework:
  stage: build
  extends:
    - .ne-macos-runner-distribution
  script: 
    - ./build_xcframework.sh
  artifacts:
    paths:
      - SampleFramework.xcframework.zip

#
# Pushes the new built artifacts to the repo for deploy, and updates the podspec with the appropriate version.
#
# Usage:
#  - NESDKIOS_VERSION: the new version of the iOS SDK .e.g 3.1.4
#  - NESDKIOS_UPSTREAM_PIPELINE: The pipeline ID for the iOS SDK pipeline to pull artifacts from.  Note the jobs must have succeeded.
#
Update Version:
  extends: .base-update-repo
  stage: update
  rules:
    - if: $CI_PIPELINE_SOURCE == "pipeline" && $NESDKIOS_VERSION != null && $NESDKIOS_VERSION != "" 
    - if: $CI_PIPELINE_SOURCE == "web" && $NESDKIOS_VERSION != "" 
    - if: $NESDKIOS_VERSION != null && $NESDKIOS_VERSION != "" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  needs:
    - job: Get Artifacts NumberEightCompiled
      artifacts: true
      optional: true
  variables:
    UPSTREAM_PROJECT_ID: $NESDKIOS_PROJECT_ID
    UPDATE_VERSION: $NESDKIOS_VERSION
  script:
    - sed -i -e "s/version = '.*'/version = '${UPDATE_VERSION:1}'/" $SAMPLE_FRAMEWORK_APP_PODFILE
    - sed -i -e "s/  spec.version = '.*'/  spec.version = '${UPDATE_VERSION:1}'/" $SAMPLE_FRAMEWORK_PODSPEC
    - git add $SAMPLE_FRAMEWORK_PODSPEC $SAMPLE_FRAMEWORK_APP_PODFILE
    - !reference [.push-to-git, script]

#
# Lints the CocoaPod before we do the release.  This is idempotent.
#
Lint SampleFramework:
  extends: .baseLintCocoapod
  stage: lint
  variables:
    PODSPEC: $SAMPLE_FRAMEWORK_PODSPEC
  needs:
    - job: Build SampleFramework
      artifacts: true
  before_script:
    - unzip -o SampleFramework.xcframework.zip
    - rm SampleFramework.xcframework.zip

#
# Pushes SampleFramework to the numbereight CocoaPods repo.
# Don't push to trunk.
#
Push SampleFramework:
  extends: .basePushToCocoapods
  stage: deploy
  needs:
    - job: Lint SampleFramework
      artifacts: false
  variables:
    DESTINATION: "numbereight"
    PODSPEC: $SAMPLE_FRAMEWORK_PODSPEC
